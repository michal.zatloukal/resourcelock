﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceSynchronizer
{
    public class Command
    {
        public int SequenceNumber { get; }
        public string ResourceId { get; }
        public Command(int sequenceNumber, string resourceId)
        {
            SequenceNumber = sequenceNumber;
            ResourceId = resourceId;
        }
    }
}
