﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace DeviceSynchronizer
{
    public class ResourceLock : IDisposable
    {
        private string _resourceId;
        private Action<string> _action;

        public ResourceLock(string resourceId, Action<string> action)
        {
            _resourceId = resourceId;
            _action = action;
        }

        public void Dispose() => _action(_resourceId);
    }

    public class ResourceLockSource
    {
        private readonly TaskCompletionSource<ResourceLock>? _tcs;
        private readonly Task<ResourceLock> _task;
        private readonly ResourceLock _resourceLock;
        public int CommandId { get; }

        public ResourceLockSource(int commandId, string resourceId, Action<string> releaseResource, bool accessReady = false)
        {
            CommandId = commandId;
            _resourceLock = new ResourceLock(resourceId, releaseResource);

            if (accessReady)
            {
                _task = Task.FromResult(_resourceLock);
            }
            else
            {
                _tcs = new();
                _task = _tcs.Task;
            }
        }

        public Task<ResourceLock> GetLock() => _task;

        public void AllowAccessToResource()
        {
            _tcs?.SetResult(_resourceLock);
        }
    }

    public class Synchronizer
    {
        private readonly ConcurrentDictionary<string, Channel<ResourceLockSource>> _resources = new();

        private readonly SemaphoreSlim _semaphor = new SemaphoreSlim(5,5);

        // Assumption: Called sequentially, not enterily threadsafe.
        public async Task<ResourceLockSource> UseResourceAsync(int commandId, string resourceId)
        {
            await _semaphor.WaitAsync();

            var rlq = _resources.GetOrAdd(resourceId, key => Channel.CreateUnbounded<ResourceLockSource>());

            if (rlq.Reader.Count is 0)
            {
                var rls = new ResourceLockSource(commandId, resourceId, (rid) => ReleaseResource(rid, true), true);
                await rlq.Writer.WriteAsync(rls);
                Console.WriteLine("Command " + commandId + " written (A)");
                return rls;
            }
            else
            {
                var rls = new ResourceLockSource(commandId, resourceId, (rid) => ReleaseResource(rid));
                await rlq.Writer.WriteAsync(rls);
                Console.WriteLine("Command " + commandId + " written");
                return rls;
            }
        }

        private void ReleaseResource(string resourceId, bool removeItself = false)
        {
            try
            {
                var rlq = _resources[resourceId];

                //Remove itself from the queue
                if (removeItself)
                {
                    if (rlq.Reader.TryRead(out var rls1))
                    {
                        Console.WriteLine("Removed resource " + resourceId + $" (command {rls1.CommandId})");
                    }
                }

                //TODO: Issue when last item is removed & allowed - it starts to run
                // but in the meantime the channel is empty so new resource is granted
                //Allow the next resource
                if (rlq.Reader.TryRead(out var rls))
                {
                    Console.WriteLine("Removed and allowed " + resourceId + $" (command {rls.CommandId})");
                    rls.AllowAccessToResource();
                }
            }
            finally
            {
                _semaphor.Release();
            }
        }

        internal async Task CompleteAsync()
        {
            foreach (var (_, channel) in _resources)
            {
                channel.Writer.Complete();
            }

            foreach (var (_, channel) in _resources)
            {
                await channel.Reader.Completion;
            }
        }
    }

    public class Program
    {
        static async Task Main(string[] args)
        {
            Channel<Command> channel = Channel.CreateUnbounded<Command>();
            _ = Task.Run(async () =>
            {
                int sequenceNumber = 0;
                while(true)
                {
                    sequenceNumber++;
                    await channel.Writer.WriteAsync(new Command(sequenceNumber, "1"));
                    sequenceNumber++;
                    await channel.Writer.WriteAsync(new Command(sequenceNumber, "2"));
                    await Task.Delay(100);
                }
            });

            var synchronizer = new Synchronizer();

            await foreach (var command in channel.Reader.ReadAllAsync())
            {
                var resourceLockSource = await synchronizer.UseResourceAsync(command.SequenceNumber, command.ResourceId);
                _ = Task.Run(async () =>
                {
                    try
                    {
                        //Console.WriteLine("Waiting to perform command " + command.SequenceNumber);
                        using (var resourceLock = await resourceLockSource.GetLock())
                        {
                            Console.WriteLine($"{DateTime.UtcNow.ToString("HH:mm:ss")} - Started performing command {command.SequenceNumber} for resource: {command.ResourceId}");
                            // do Command
                            if (command.ResourceId == "1")
                            {
                                await Task.Delay(2000);
                            }
                            else
                            {
                                await Task.Delay(3000);
                            }
                            Console.WriteLine($"{DateTime.UtcNow.ToString("HH:mm:ss")} - Performed command {command.SequenceNumber} for resource: {command.ResourceId}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                });
            }

            //await synchronizer.CompleteAsync();
        }
    }


}
